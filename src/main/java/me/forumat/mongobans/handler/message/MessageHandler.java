package me.forumat.clansystem.handler.message;

import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import java.io.File;
import java.util.Objects;
import java.util.regex.Matcher;

public class MessageHandler {

    private File file;
    private YamlConfiguration yamlConfiguration;
    private String prefixKey;


    public MessageHandler(File file, String prefixKey) {
        this.file = file;
        yamlConfiguration = YamlConfiguration.loadConfiguration(file);
        this.prefixKey = translateColorCodesPlus(yamlConfiguration.getString(prefixKey));
    }

    public String getMessage(String messageKey, Object... args) {
        String message = yamlConfiguration.getString(messageKey);
        if (message == null) {
            message = "§cNachricht " + messageKey + " wurde nicht gefunden!";
        }
        message = message.replace("%P%", prefixKey);
        if (args.length != 0) {
            int i = 0;
            for (Object arg : args) {
                if (arg != null) {
                    message = message.replaceAll("\\{" + i + "}", Matcher.quoteReplacement(arg.toString()));
                    i++;
                }
            }
        }
        return translateColorCodesPlus(message);
    }


    public void sendMessage(CommandSender commandSender, String key, Object... replacements){
        commandSender.sendMessage(getMessage(key, replacements));
    }

    public static String translateColorCodesPlus(String stringOfTranslate) {
        if (Objects.isNull(stringOfTranslate)) return "";
        String text = ChatColor.translateAlternateColorCodes('&', stringOfTranslate);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < text.length(); ++i) {
            if (i >= text.length() - 8) {
                sb.append(text.charAt(i));
            } else {
                String temp = text.substring(i, i + 8);
                if (temp.startsWith("&#")) {
                    try {
                        Integer.parseInt(temp.substring(2), 16);
                        sb.append("§x");
                        char[] c = temp.toCharArray();

                        for (int i1 = 2; i1 < c.length; ++i1) {
                            sb.append("§").append(c[i1]);
                        }

                        i += 7;
                    } catch (NumberFormatException ignored) {
                    }
                } else {
                    sb.append(text.charAt(i));
                }
            }
        }
        return sb.toString();
    }

}
