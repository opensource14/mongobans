package me.forumat.clansystem.handler.mongo;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.bson.Document;

public class MongoHandler {

    private MongoClient mongoClient;
    private MongoDatabase currentDatabase;

    public MongoHandler(String host, int port) {
        this.mongoClient = new MongoClient(host, port);
    }

    public MongoHandler(String connectionString){
        this.mongoClient = new MongoClient(
                new MongoClientURI(connectionString));
    }

    public void selectDatabase(String databaseName) {
        this.currentDatabase = this.mongoClient.getDatabase(databaseName);
    }

    public MongoCollection<Document> getCollection(String collectionName){
        return this.currentDatabase.getCollection(collectionName);
    }

    public void insertValue(MongoCollection collection, String key, Object value){
        if(existsKey(collection, key, value)){
            return;
        }
        Document document = new Document(key, value);
        collection.insertOne(document);
    }

    public boolean existsKey(MongoCollection collection, String key, Object setKey){
        return collection.find(Filters.eq(key, setKey)).first() != null;
    }

    public void setValue(MongoCollection collection, String keyWhere, String setKeyWhere, String keySet, Object newValue){
        if(!existsKey(collection, keyWhere, setKeyWhere)){
            return;
        }
        Document old = getValue(collection, keyWhere, setKeyWhere);
        old.put(keySet, newValue);
        collection.findOneAndReplace(Filters.eq(keyWhere, setKeyWhere), old);
    }

    public Document getValue(MongoCollection collection, String key, String keySet){
        FindIterable<Document> findIterable = collection.find(Filters.eq(key, keySet));
        for(Document document : findIterable){
            return document;
        }
        return null;
    }

    public MongoClient getMongoClient() {
        return mongoClient;
    }
}
